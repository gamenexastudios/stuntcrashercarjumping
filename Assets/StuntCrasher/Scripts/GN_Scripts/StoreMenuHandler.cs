﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreMenuHandler : MonoBehaviour
{
    public GameObject StoreMenuPanel;

    public static StoreMenuHandler instance;
    private void Awake()
    {
        instance = this;
        StoreMenuPanel.SetActive(false);
    }

    public void Open()
    {
        StoreMenuPanel.SetActive(true);
    }

    public void Close()
    {
        StoreMenuPanel.SetActive(false);
        MainMenuHandler.instance.Open();
    }
}
