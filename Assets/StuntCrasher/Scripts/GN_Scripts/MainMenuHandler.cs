﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


public class MainMenuHandler : MonoBehaviour
{
    public GameObject MainMenuPanel;


    [Header("Player")]
    public C_CarController playerVehicle;
    public C_Camera playerCamera;


    private Vector3 launchPosition;
    public C_CarController[] allPlayerVehicles;

    public int selectedVehicle = 0;
    public int selectedScene = 0;

    [Header("Upgrades")]
    public Button upgradeEngine;
    public Button upgradeBoost;
    public Button upgradeBonus;

    [Space()]

    public Text upgradeEngineLevel;
    public Text upgradeEngineLevelPrice;
    public Text upgradeBoostLevel;
    public Text upgradeBoostLevelPrice;
    public Text upgradeBonusLevel;
    public Text upgradeBonusLevelPrice;

    [Space()]

    public Text[] cashText;


    public static MainMenuHandler instance;
    private void Awake()
    {
        instance = this;
        MainMenuPanel.SetActive(true);
    }

    public void Start()
    {

        //	Getting lastly selected vehicle index.
        selectedVehicle = PlayerPrefs.GetInt("SelectedVehicle", 0);

        //	Getting lastly selected scene index.
        selectedScene = PlayerPrefs.GetInt("SelectedScene", 0);
        //	Opening main menu at the beggining.
        //OpenMenu(mainMenu);

        //	Updating all cash texts at the beginning.
        foreach (var item in cashText)
            item.text = C_API.GetCurrency().ToString("F0");

        //	Spawns and stores all player vehicles.
        //SpawnAllVehicles();


        //	Getting all selectable player vehicles from C_PlayerVehicles scriptable object located in "Resources" folder.
        allPlayerVehicles = new C_CarController[C_PlayerVehicles.Instance.vehicles.Length];

        //	Instantiate all selectable player vehicles
        for (int i = 0; i < C_PlayerVehicles.Instance.vehicles.Length; i++)
        {
            GameObject newCar = GameObject.Instantiate(C_PlayerVehicles.Instance.vehicles[i].carController.gameObject, C_GameManager.Instance.spawnPoint.position, C_GameManager.Instance.spawnPoint.rotation);
            newCar.gameObject.SetActive(false);
            allPlayerVehicles[i] = newCar.GetComponent<C_CarController>();
        }
        CarSelectionHandler.instance.SelectVehicle(selectedVehicle);
    }

    public void Open()
    {
        MainMenuPanel.SetActive(true);
    }

    public void Close()
    {
        MainMenuPanel.SetActive(false);
    }

    public void SpawnVehicle()
    {
        // Disabling all vehicles.
        for (int i = 0; i < allPlayerVehicles.Length; i++)
            allPlayerVehicles[i].gameObject.SetActive(false);

        selectedVehicle = PlayerPrefs.GetInt("SelectedVehicle", 0);
        // And only enabling selected vehicle.
        playerVehicle = allPlayerVehicles[selectedVehicle].GetComponent<C_CarController>();
        playerVehicle.isControllable = false;
        playerVehicle.gameObject.SetActive(true);
        playerVehicle.rigid.maxAngularVelocity = 10f;
        playerVehicle.rigid.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;

        //	Checks all upgrade stats of the selected vehicle.
        CheckAllUpgrades();

    }

    public void CheckAllUpgrades()
    {
        // If there are no any player vehicle, return.
        if (!playerVehicle)
            return;

        //	Updating all upgrade stats.
        upgradeEngineLevel.text = "Level " + playerVehicle.currentEngineLevel.ToString("F0"); // + " / 100";
        upgradeBoostLevel.text = "Level " + playerVehicle.currentBoostLevel.ToString("F0"); // + " / 100";
        upgradeBonusLevel.text = "Level " + playerVehicle.currentBonusLevel.ToString("F0"); //+ " / 100";

        upgradeEngineLevelPrice.text = playerVehicle.nextPriceForEngineUpgrade.ToString("F0");
        upgradeBoostLevelPrice.text = playerVehicle.nextPriceForBoostUpgrade.ToString("F0");
        upgradeBonusLevelPrice.text = playerVehicle.nextPriceForBonusUpgrade.ToString("F0");

        int currentCash = C_API.GetCurrency();  //	Getting current cash.

        //	If current cash is more than target price of the upgrade, make the button interactable.
        if (currentCash >= playerVehicle.nextPriceForEngineUpgrade && playerVehicle.currentEngineLevel < 100)
            upgradeEngine.interactable = true;
        else
            upgradeEngine.interactable = false;

        //	If current cash is more than target price of the upgrade, make the button interactable.
        if (currentCash >= playerVehicle.nextPriceForBoostUpgrade && playerVehicle.currentBoostLevel < 100)
            upgradeBoost.interactable = true;
        else
            upgradeBoost.interactable = false;

        //	If current cash is more than target price of the upgrade, make the button interactable.
        if (currentCash >= playerVehicle.nextPriceForBonusUpgrade && playerVehicle.currentBonusLevel < 100)
            upgradeBonus.interactable = true;
        else
            upgradeBonus.interactable = false;
    }

    public void SpawnAllVehicles()
    {

        //	Getting all selectable player vehicles from C_PlayerVehicles scriptable object located in "Resources" folder.
        allPlayerVehicles = new C_CarController[C_PlayerVehicles.Instance.vehicles.Length];

        //	Instantiate all selectable player vehicles
        for (int i = 0; i < C_PlayerVehicles.Instance.vehicles.Length; i++)
        {

            GameObject newCar = GameObject.Instantiate(C_PlayerVehicles.Instance.vehicles[i].carController.gameObject, C_GameManager.Instance.spawnPoint.position, C_GameManager.Instance.spawnPoint.rotation);
            newCar.gameObject.SetActive(false);
            allPlayerVehicles[i] = newCar.GetComponent<C_CarController>();

        }

        //	Getting lastly selected vehicle index.
        selectedVehicle = PlayerPrefs.GetInt("SelectedVehicle", 0);

        //	Getting lastly selected scene index.
        selectedScene = PlayerPrefs.GetInt("SelectedScene", 0);

        //	Spawns selected vehicle.
        SpawnVehicle();

    }


    public void UpgradeEngine()
    {

        // Getting and saving upgrade state of the vehicle.
        int currentUpgradeLevel = playerVehicle.currentEngineLevel;

        if (currentUpgradeLevel >= 100)
            return;

        //	Consumes currency.
        C_API.ConsumeCurrency(playerVehicle.nextPriceForEngineUpgrade);

        currentUpgradeLevel += 1;
        PlayerPrefs.SetInt(playerVehicle.transform.name + "_EngineLevel", currentUpgradeLevel);

        // Checks all upgrades after purchasing.
        CheckAllUpgrades();

    }

    /// <summary>
    /// Upgrades the boost.
    /// </summary>
    public void UpgradeBoost()
    {

        // Getting and saving upgrade state of the vehicle.
        int currentUpgradeLevel = playerVehicle.currentBoostLevel;

        if (currentUpgradeLevel >= 100)
            return;

        //	Consumes currency.
        C_API.ConsumeCurrency(playerVehicle.nextPriceForBoostUpgrade);


        currentUpgradeLevel += 1;
        PlayerPrefs.SetInt(playerVehicle.transform.name + "_BoostLevel", currentUpgradeLevel);

        // Checks all upgrades after purchasing.
        CheckAllUpgrades();

    }

    /// <summary>
    /// Upgrades the bonus.
    /// </summary>
    public void UpgradeBonus()
    {

        // Getting and saving upgrade state of the vehicle.
        int currentUpgradeLevel = playerVehicle.currentBonusLevel;

        if (currentUpgradeLevel >= 100)
            return;

        //	Consumes currency.
        C_API.ConsumeCurrency(playerVehicle.nextPriceForBonusUpgrade);

        currentUpgradeLevel += 1;
        PlayerPrefs.SetInt(playerVehicle.transform.name + "_BonusLevel", currentUpgradeLevel);

        // Checks all upgrades after purchasing.
        CheckAllUpgrades();

    }

    /// <summary>
    /// Starts the game.
    /// </summary>
    public void StartGame()
    {
        Close();
        InGameMenuHandler.instance.Open();

        if (C_GameManager.Instance != null)
        {
            C_GameManager.Instance.StartGame();
        }
    }


    // TESTING METHODS
    /// <summary>
    /// Adds the money.
    /// </summary>
    public void AddMoney()
    {
        C_API.AddCurrency(10000);
    }

    /// <summary>
    /// Unlocks all vehicles.
    /// </summary>
    public void UnlockVehicles()
    {
        for (int i = 0; i < C_PlayerVehicles.Instance.vehicles.Length; i++)
        {
            C_PlayerPrefsX.SetBool(C_PlayerVehicles.Instance.vehicles[i].carController.name, true);
        }
        SpawnVehicle();
    }

    /// <summary>
    /// Unlocks all levels.
    /// </summary>
    public void UnlockLevels()
    {
        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            C_API.UnlockLevel(i);
        }
    }

    /// <summary>
    /// Resets everything and reloads the scene.
    /// </summary>
    public void Reset()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
