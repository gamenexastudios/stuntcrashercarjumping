﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuHandler : MonoBehaviour
{
    public GameObject SettingsMenuPanel;
    
    [Header("SOUNDS ASSETS")]
    public Image SoundImage;
    public Sprite soundOn;
    public Sprite soundOff;

    [Header("MUSIC ASSETS")]
    public Image MusicImage;
    public Sprite musicOn;
    public Sprite musicOff;

    [Header("VIBRATION ASSETS")]
    public Image VibrationImage;
    public Sprite vibrationOn;
    public Sprite vibrationOff;

    [Header("QUALITY ASSETS")]
    public Image QualityImage;
    public Sprite qualityHigh;
    public Sprite qualityLow;

    public static SettingsMenuHandler instance;
    private void Awake()
    {
        instance = this;
        SettingsMenuPanel.SetActive(false);
    }

    void OnEnable()
    {
        if (GamePrefs._iSoundStatus == 1)
            SoundImage.sprite = soundOn;
        else
            SoundImage.sprite = soundOff;


        if (GamePrefs._iMusicStatus == 1)
            MusicImage.sprite = musicOn;
        else
            MusicImage.sprite = musicOff;


        if (GamePrefs._iVibrationStatus == 1)
            VibrationImage.sprite = vibrationOn;
        else
            VibrationImage.sprite = vibrationOff;


        if (GamePrefs._iGraphicsStatus == 1)
            QualityImage.sprite = qualityHigh;
        else
            QualityImage.sprite = qualityLow;
    }


    public void Open()
    {
        SettingsMenuPanel.SetActive(true);
    }

    public void Close()
    {
        SettingsMenuPanel.SetActive(false);
        MainMenuHandler.instance.Open();
    }

    
    public void SoundsBtn_Click()
    {
        if (GamePrefs._iSoundStatus == 1)
        {
            GamePrefs._iSoundStatus = 0;
            AudioListener.volume = 0;
            SoundImage.sprite = soundOff;

        }
        else
        {
            GamePrefs._iSoundStatus = 1;
            AudioListener.volume = 1;
            SoundImage.sprite = soundOn;
        }
    }

    public void MusicBtn_Click()
    {
        if (GamePrefs._iMusicStatus == 1)
        {
            GamePrefs._iMusicStatus = 0;
            AudioListener.volume = 0;
            MusicImage.sprite = musicOff;
        }
        else
        {
            GamePrefs._iMusicStatus = 1;
            AudioListener.volume = 1;
            MusicImage.sprite = musicOn;
        }
    }

    public void VibrationBtn_Click()
    {
        if (GamePrefs._iVibrationStatus == 1)
        {
            GamePrefs._iVibrationStatus = 0;
            VibrationImage.sprite = vibrationOff;
        }
        else
        {
            GamePrefs._iVibrationStatus = 1;
            VibrationImage.sprite = vibrationOn;
        }
    }


    public void GraphicsBtn_Click()
    {
        if (GamePrefs._iGraphicsStatus == 1)
        {
            GamePrefs._iGraphicsStatus = 0;
            QualityImage.sprite = qualityLow;
        }
        else
        {
            GamePrefs._iGraphicsStatus = 1;
            QualityImage.sprite = qualityHigh;
        }
    }
}
