﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompletionHandler : MonoBehaviour
{
    public GameObject LevelCompletePanel;

    public static LevelCompletionHandler instance;
    private void Awake()
    {
        instance = this;
        LevelCompletePanel.SetActive(false);
    }

    public void Open()
    {
        if (C_GameManager.Instance != null && C_GameManager.Instance.score > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", C_GameManager.Instance.score);
        }
        InGameMenuHandler.instance.HighScoreText.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        LevelCompletePanel.SetActive(true);
    }

    public void Close()
    {
        LevelCompletePanel.SetActive(false);
    }

    public void LoadNextScene()
    {
        if (C_GameManager.Instance != null)
        {
            if (C_GameManager.Instance.gameCompleted)
            {
                C_GameManager.Instance.CheckAndLoadNextScene();
            }
            else
            {
                InGameMenuHandler.instance.Restart();
            }
        }
    }

    public void WatchVideoToDoubleYourCoins()
    {

    }
}
