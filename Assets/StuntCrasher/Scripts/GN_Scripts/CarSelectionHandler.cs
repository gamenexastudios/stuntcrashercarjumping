﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarSelectionHandler : MonoBehaviour
{
    public GameObject CarSelectionPanel;

    public GameObject[] CarPanel;
    public Sprite goSprite;
    public Sprite buySprite;
    public Sprite lockSprite;

    public Sprite CarUnlockedBG;
    public Sprite CarLockedBG;
    public Sprite TopBarUnlockedBG;
    public Sprite TopBarLockedBG;


    public static CarSelectionHandler instance;
    private void Awake()
    {
        instance = this;
        CarSelectionPanel.SetActive(false);
    }

    public void Open()
    {
        CarSelectionPanel.SetActive(true);
        VehicleUpgradeStatus();
    }

    public void Close()
    {
        CarSelectionPanel.SetActive(false);
        MainMenuHandler.instance.Open();
    }

    public void SelectVehicle(int VehicleIndex)
    {
        PlayerPrefs.SetInt("SelectedVehicle", VehicleIndex); //	Saves lastly selected vehicle index.
        MainMenuHandler.instance.playerVehicle = MainMenuHandler.instance.allPlayerVehicles[VehicleIndex];   //	Assings selected vehicle.
        MainMenuHandler.instance.SpawnVehicle();
        Close();
    }

    public void VehicleUpgradeStatus()
    {
        for (int i = 0; i < CarPanel.Length; i++)
        {
            if (C_PlayerVehicles.Instance.vehicles[i].price <= 0)
                C_PlayerPrefsX.SetBool(C_PlayerVehicles.Instance.vehicles[i].carController.name, true);

            // Enablind/ disabling select/buy buttons.
            bool vehicleUnlocked = C_PlayerPrefsX.GetBool(C_PlayerVehicles.Instance.vehicles[i].carController.name, false);
         
            if (vehicleUnlocked)
            {

                CarPanel[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = CarUnlockedBG;
                CarPanel[i].transform.GetChild(1).GetComponentInChildren<Image>().sprite = TopBarUnlockedBG;
                //CarPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Button>().gameObject.SetActive(true);
                CarPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Button>().interactable = true;
                CarPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = goSprite;

            }
            else
            {
                //buyVehicleButton.GetComponentInChildren<Text>().text = "Buy Vehicle " + C_PlayerVehicles.Instance.vehicles[selectedVehicle].price.ToString();

                if (C_API.GetCurrency() < C_PlayerVehicles.Instance.vehicles[i].price)
                {
                    CarPanel[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = CarLockedBG;
                    CarPanel[i].transform.GetChild(1).GetComponentInChildren<Image>().sprite = TopBarLockedBG;
                    CarPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Button>().interactable = false;
                    CarPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = lockSprite;
                    //CarPanel[i].transform.GetChild(0).GetChild(1).GetComponent<Button>().gameObject.SetActive(true);
                    //CarPanel[i].transform.GetChild(0).GetChild(1).GetComponent<Button>().interactable = false;
                    //CarPanel[i].transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = lockSprite;


                    //selectCarBtn[i].interactable = false;
                    //selectCarBtn[i].GetComponent<Image>().sprite = lockSprite;
                }
                else
                {
                    CarPanel[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = CarLockedBG;
                    CarPanel[i].transform.GetChild(1).GetComponentInChildren<Image>().sprite = TopBarLockedBG;
                    CarPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Button>().interactable = true;
                    CarPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = buySprite;
                    //CarPanel[i].transform.GetChild(0).GetChild(1).GetComponent<Button>().gameObject.SetActive(false);
                    //CarPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Button>().gameObject.SetActive(true);
                    //CarPanel[i].transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = buySprite;


                    //selectCarBtn[i].interactable = true;
                    //selectCarBtn[i].GetComponent<Image>().sprite = buySprite;
                }
            }

        }
    }

    public void BuyVehicle(int vehicleNo)
    {
        int currentCash = C_API.GetCurrency();

        //	If current cash is enough, purchase it. Otherwise, it fails.
        if (currentCash >= C_PlayerVehicles.Instance.vehicles[vehicleNo].price)
        {

            C_PlayerPrefsX.SetBool(C_PlayerVehicles.Instance.vehicles[vehicleNo].carController.name, true);
            C_API.ConsumeCurrency(C_PlayerVehicles.Instance.vehicles[vehicleNo].price);
            MainMenuHandler.instance.SpawnVehicle();
        }
    }

    public void VehicleSelectOrBuyBtnClick(int vehicleNo)
    {
       
        if (CarPanel[vehicleNo].gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite == buySprite)
        {
            BuyVehicle(vehicleNo);
        }
        else if (CarPanel[vehicleNo].gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite == goSprite)
        {
            SelectVehicle(vehicleNo);
        }
        else
        {
            
        }
    }

}
