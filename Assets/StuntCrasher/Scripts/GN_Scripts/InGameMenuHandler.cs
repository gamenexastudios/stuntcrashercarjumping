﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InGameMenuHandler : MonoBehaviour
{
    public GameObject InGameMenuPanel;

    [Space()]

    [Header("UI")]
    public Text[] speedText;
    public Text[] scoreText;
    public Text[] launchspeedText;
    public Text[] distanceText;
    public Text[] maxDistanceText;
    public Text[] destructionText;
    public Text[] bonusText;
    public Text HighScoreText;


    [Space()]

    public Image launchSpeedSlider;
    public Slider distanceSlider;

    public GameObject speedNeedle;
    public float speedNeedleMinAngle = 0f;
    public float speedNeedleMaxAngle = 300f;


    public static InGameMenuHandler instance;
    private void Awake()
    {
        instance = this;
        InGameMenuPanel.SetActive(false);
    }

    public void Open()
    {
        InGameMenuPanel.SetActive(true);
        MainMenuHandler.instance.Close();
    }

    public void Close()
    {
        InGameMenuPanel.SetActive(false);
    }

    /// <summary>
    /// Restart the scene.
    /// </summary>
    public void Restart()
    {
        // Reloads the same scene.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        C_GameManager.carIsGrounded = false;

    }
}
