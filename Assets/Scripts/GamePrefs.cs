﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePrefs : MonoBehaviour
{

    public static int _iSoundStatus
    {
        set
        {
            PlayerPrefs.SetInt("SoundStatus", value);
        }
        get
        {
            return PlayerPrefs.GetInt("SoundStatus", 1);
        }
    }

    public static int _iMusicStatus
    {
        set
        {
            PlayerPrefs.SetInt("MusicStatus", value);
        }
        get
        {
            return PlayerPrefs.GetInt("MusicStatus", 1);
        }
    }

    public static int _iVibrationStatus
    {
        set
        {
            PlayerPrefs.SetInt("VibrationStatus", value);
        }
        get
        {
            return PlayerPrefs.GetInt("VibrationStatus", 1);
        }
    }

    public static int _iGraphicsStatus
    {
        set
        {
            PlayerPrefs.SetInt("GraphicStatus", value);
        }
        get
        {
            return PlayerPrefs.GetInt("GraphicStatus", 1);
        }
    }
}
