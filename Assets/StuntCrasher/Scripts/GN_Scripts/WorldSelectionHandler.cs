﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class WorldSelectionHandler : MonoBehaviour
{
    public GameObject WorldSelectionPanel;

    public GameObject[] WorldPanel;
    public Sprite selectSprite;
    public Sprite lockSprite;
    public Sprite LevelUnlockedBG;
    public Sprite LevelLockedBG;
    public Sprite TopBarUnlockedBG;
    public Sprite TopBarLockedBG;

    public static WorldSelectionHandler instance;
    private void Awake()
    {
        instance = this;
        WorldSelectionPanel.SetActive(false);
    }

    public void Open()
    {
        WorldSelectionPanel.SetActive(true);

        for (int i = 0; i < WorldPanel.Length; i++)
        {
            if (C_API.CheckLevel(i))
            {
                Debug.LogError(i + " LEVEL VALUE is : TRUE");
                WorldPanel[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = LevelUnlockedBG;
                WorldPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Button>().interactable = true;
                WorldPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = selectSprite;
                WorldPanel[i].transform.GetChild(1).GetComponentInChildren<Image>().sprite = TopBarUnlockedBG;
            }
            else
            {
                Debug.LogError(i + " LEVEL VALUE is : FALSE");
                WorldPanel[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = LevelLockedBG;
                WorldPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Button>().interactable = false;
                WorldPanel[i].transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = lockSprite;
                WorldPanel[i].transform.GetChild(1).GetComponentInChildren<Image>().sprite = TopBarLockedBG;
            }
        }
    }

    public void Close()
    {
        WorldSelectionPanel.SetActive(false);
        MainMenuHandler.instance.Open();
    }
    public void SelectScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
        PlayerPrefs.SetInt("SelectedScene", sceneIndex);

        //bool sceneUnlocked = C_API.CheckLevel(sceneIndex);

        //if (sceneUnlocked)
        //{
        //    PlayerPrefs.SetInt("SelectedScene", sceneIndex);
        //    SceneManager.LoadScene(sceneIndex);
        //}
        //else
        //{
        //    lockedSceneMenu.SetActive(true);
        //}
    }
}
